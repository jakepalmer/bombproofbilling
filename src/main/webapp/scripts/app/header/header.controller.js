/**
 * Created by jake on 9/12/15.
 */
'use strict';

angular.module('bombproofbillingApp')
.controller("HeaderController", function($rootScope, $scope, $timeout, Fullscreen, Auth, $state, $location) {
    $scope.toggleFloatingSidebar = function() {
        $scope.floatingSidebar = $scope.floatingSidebar ? false : true;
        console.log("floating-sidebar: " + $scope.floatingSidebar);
    };
    $scope.goFullScreen = function() {
        if (Fullscreen.isEnabled())
            Fullscreen.cancel();
        else
            Fullscreen.all()
    };

    $scope.logout = function(){
        Auth.logout();
        $location.path('/login')
    };


});
