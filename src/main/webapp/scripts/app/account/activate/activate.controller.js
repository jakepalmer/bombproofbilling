'use strict';

angular.module('bombproofbillingApp')
    .controller('ActivationController', function ($scope, $stateParams, Auth) {

        $scope.password = null;
        $scope.passwordConfirm = null;
        $scope.email = null;
        $scope.errorMessage = "User could not be activated";
        $scope.activate = function() {
            if ($scope.password === $scope.passwordConfirm) {

                Auth.activateAccount({
                    key: $stateParams.key,
                    email: $scope.email,
                    password: $scope.password
                }).then(function () {
                    $scope.error = null;
                    $scope.success = 'OK';
                }).catch(function () {
                    $scope.success = null;
                    $scope.error = 'ERROR';
                });
            } else {
                $scope.error = 'ERROR';
                $scope.errorMessage = "Passwords do not match";
            }
        }
    });

