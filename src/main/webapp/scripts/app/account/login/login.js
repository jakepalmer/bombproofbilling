'use strict';

angular.module('bombproofbillingApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('login', {
                parent: 'site',
                url: '/login',
                data: {
                    roles: [],
                    pageTitle: 'Login'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/account/login/signin.html',
                        controller: 'LoginController'
                    }
                },
                resolve: {

                }
            });
    });
