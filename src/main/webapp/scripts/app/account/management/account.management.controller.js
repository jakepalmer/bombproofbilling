'use strict';

angular.module('bombproofbillingApp')
    .controller('AccountManagementController', function ($scope, $stateParams, Auth, Principal, Client, Trainer, User ,Admin) {
        Principal.identity().then(function(account) {
            $scope.settingsAccount = account;
        });
        $scope.user = function() {
            Principal.identity(true).then(function(account) {
                $scope.user = account;
            });
        };
        $scope.collapse = true;
        $scope.toggleText = function() {
            if ($scope.collapse) {
                return "More";
            } else {
                return "Less";
            }
        };
        $scope.clients = [];
        $scope.trainers = [];
        $scope.admins = [];
        $scope.loadAll = function() {
            Client.query(function(result) {
                $scope.clients = result;
            });
            Trainer.query(function(result) {
                $scope.trainers = result;
            });
            Admin.getAdmins(function(result){
                $scope.admins = result;
            });
        };


        $scope.loadAll();


        $scope.client = {};

        $scope.deleteClient = function (id) {
            Client.get({id: id}, function(result) {
                $scope.client = result;
                $('#deleteClientConfirmation').modal('show');
            });
        };

        $scope.confirmDeleteClient = function (id) {
            Client.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteClientConfirmation').modal('hide');
                });
        };

        $scope.trainer = {};

        //Trainer account deletion
        $scope.deleteTrainer = function (id) {
            Trainer.get({id: id}, function(result) {
                $scope.trainer = result;
                $('#deleteTrainerConfirmation').modal('show');
            });
        };

        $scope.confirmDeleteTrainer = function (id) {
            Trainer.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTrainerConfirmation').modal('hide');
                });
        };

        $scope.admin = {};

        //Trainer account deletion
        $scope.deleteAdmin = function (login) {
            Admin.get({login: login}, function(result) {
                $scope.admin = result;
                $('#deleteAdminConfirmation').modal('show');
            });
        };

        $scope.confirmDeleteAdmin = function (id) {
            console.log(id);
            Admin.deleteAdmin({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAdminConfirmation').modal('hide');
                });
        };
    });

