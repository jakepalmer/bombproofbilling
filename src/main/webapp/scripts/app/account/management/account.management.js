'use strict';

angular.module('bombproofbillingApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('accountmanagement', {
                parent: 'home',
                url: 'accountmanagement',
                data: {
                    roles: ['ROLE_ADMIN'],
                    pageTitle: 'Account Management'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/account/management/account_management.html',
                        controller: 'AccountManagementController'
                    }
                },
                resolve: {

                }
            })
            .state('client.detail', {
                parent: 'accountmanagement',
                url: '/accountmanagement/client/{id}',
                data: {
                    roles: ['ROLE_TRAINER', 'ROLE_ADMIN'],
                    pageTitle: 'Client'
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    var id = $stateParams.id;
                    $modal.open({
                        templateUrl: 'scripts/app/entities/client/client-detail.html',
                        controller: 'ClientDetailController',
                        size: 'lg',
                        resolve: {
                            entity: ['Client', function(Client) {
                                return Client.get({id : id});
                            }]
                        }
                    })
                }]
            })
            .state('client.edit', {
                parent: 'accountmanagement',
                url: '/accountmanagement/client/{id}/edit',
                data: {
                    roles: ['ROLE_TRAINER', 'ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/client/client-dialog.html',
                        controller: 'ClientDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Client', function(Client) {
                                return Client.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                            $state.go('accountmanagement', null, { reload: true });
                        }, function() {
                            $state.go('^');
                        })
                }]
            })
            .state('trainer.detail', {
                parent: 'accountmanagement',
                url: '/accountmanagement/trainer/{id}',
                data: {
                    roles: ['ROLE_ADMIN'],
                    pageTitle: 'Trainer'
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    var id = $stateParams.id;
                    $modal.open({
                        templateUrl: 'scripts/app/entities/trainer/trainer-detail.html',
                        controller: 'TrainerDetailController',
                        size: 'lg',
                        resolve: {
                            entity: ['$stateParams', 'Trainer', function($stateParams, Trainer) {
                                return Trainer.get({id : id});
                            }]
                        }
                    })
                }]

            })
            .state('trainer.edit', {
                parent: 'accountmanagement',
                url: '/accountmanagement/{id}/edit',
                data: {
                    roles: ['ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/trainer/trainer-dialog.html',
                        controller: 'TrainerDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Trainer', function(Trainer) {
                                return Trainer.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                            $state.go('accountmanagement', null, { reload: true });
                        }, function() {
                            $state.go('^');
                        })
                }]
            })
            .state('admin.detail', {
                parent: 'accountmanagement',
                url: '/accountmanagement/admin/{id}',
                data: {
                    roles: ['ROLE_ADMIN'],
                    pageTitle: 'Admin'
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    var id = $stateParams.id;
                    $modal.open({
                        templateUrl: 'scripts/app/entities/admin/admin-detail.html',
                        controller: 'AdminDetailController',
                        size: 'lg',
                        resolve: {
                            entity: ['$stateParams', 'Admin', function($stateParams, Admin) {
                                return Admin.getById({id : id});
                            }]
                        }
                    })
                }]

            })
            .state('admin.edit', {
                parent: 'accountmanagement',
                url: '/accountmanagement/{id}/edit',
                data: {
                    roles: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    var id = $stateParams.id;
                    $modal.open({
                        templateUrl: 'scripts/app/entities/admin/admin-dialog.html',
                        controller: 'AdminDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Admin', function(Admin) {
                                return Admin.getById({id : id});
                            }]
                        }
                    }).result.then(function(result) {
                            $state.go('accountmanagement', null, { reload: true });
                        }, function() {
                            $state.go('^');
                        })
                }]
            });

    });

