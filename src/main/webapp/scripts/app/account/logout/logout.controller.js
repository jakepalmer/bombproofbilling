'use strict';

angular.module('bombproofbillingApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
