'use strict';

angular.module('bombproofbillingApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('horse', {
                parent: 'entity',
                url: '/horses',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Horses'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/horse/horses.html',
                        controller: 'HorseController'
                    }
                },
                resolve: {
                }
            })
            .state('horse.detail', {
                parent: 'entity',
                url: '/horse/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Horse'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/horse/horse-detail.html',
                        controller: 'HorseDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Horse', function($stateParams, Horse) {
                        return Horse.get({id : $stateParams.id});
                    }]
                }
            })
            .state('horse.new', {
                parent: 'horse',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/horse/horse-dialog.html',
                        controller: 'HorseDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {horseId: null, name: null, breed: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('horse', null, { reload: true });
                    }, function() {
                        $state.go('horse');
                    })
                }]
            })
            .state('horse.edit', {
                parent: 'horse',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/horse/horse-dialog.html',
                        controller: 'HorseDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Horse', function(Horse) {
                                return Horse.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('horse', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
