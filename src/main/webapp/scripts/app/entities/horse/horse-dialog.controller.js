'use strict';

angular.module('bombproofbillingApp').controller('HorseDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Horse', 'Client',
        function($scope, $stateParams, $modalInstance, entity, Horse, Client) {

        $scope.horse = entity;
        $scope.clients = Client.query();
        $scope.load = function(id) {
            Horse.get({id : id}, function(result) {
                $scope.horse = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('bombproofbillingApp:horseUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.horse.id != null) {
                Horse.update($scope.horse, onSaveFinished);
            } else {
                Horse.save($scope.horse, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
