'use strict';

angular.module('bombproofbillingApp')
    .controller('HorseDetailController', function ($scope, $rootScope, $stateParams, entity, Horse, Client) {
        $scope.horse = entity;
        $scope.load = function (id) {
            Horse.get({id: id}, function(result) {
                $scope.horse = result;
            });
        };
        $rootScope.$on('bombproofbillingApp:horseUpdate', function(event, result) {
            $scope.horse = result;
        });
    });
