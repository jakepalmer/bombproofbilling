'use strict';

angular.module('bombproofbillingApp')
    .controller('HorseController', function ($scope, Horse) {
        $scope.horses = [];
        $scope.loadAll = function() {
            Horse.query(function(result) {
               $scope.horses = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Horse.get({id: id}, function(result) {
                $scope.horse = result;
                $('#deleteHorseConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Horse.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteHorseConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.horse = {horseId: null, name: null, breed: null, id: null};
        };
    });
