'use strict';

angular.module('bombproofbillingApp').controller('ServiceDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Service', 'ServiceCategory',
        function($scope, $stateParams, $modalInstance, entity, Service, ServiceCategory) {

        $scope.service = entity;
        $scope.servicecategorys = ServiceCategory.query();
        $scope.load = function(id) {
            Service.get({id : id}, function(result) {
                $scope.service = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('bombproofbillingApp:serviceUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.service.id != null) {
                Service.update($scope.service, onSaveFinished);
            } else {
                Service.save($scope.service, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
