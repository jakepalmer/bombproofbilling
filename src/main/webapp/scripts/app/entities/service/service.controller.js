'use strict';

angular.module('bombproofbillingApp')
    .controller('ServiceController', function ($scope, Service) {
        $scope.services = [];
        $scope.loadAll = function() {
            Service.query(function(result) {
               $scope.services = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Service.get({id: id}, function(result) {
                $scope.service = result;
                $('#deleteServiceConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Service.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteServiceConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.service = {name: null, id: null};
        };
    });
