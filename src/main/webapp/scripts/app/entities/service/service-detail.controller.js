'use strict';

angular.module('bombproofbillingApp')
    .controller('ServiceDetailController', function ($scope, $rootScope, $stateParams, entity, Service, ServiceCategory) {
        $scope.service = entity;
        $scope.load = function (id) {
            Service.get({id: id}, function(result) {
                $scope.service = result;
            });
        };
        $rootScope.$on('bombproofbillingApp:serviceUpdate', function(event, result) {
            $scope.service = result;
        });
    });
