'use strict';

angular.module('bombproofbillingApp').controller('CustomServiceDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'CustomService', 'Trainer',
        function($scope, $stateParams, $modalInstance, entity, CustomService, Trainer) {

        $scope.customService = entity;
        $scope.trainers = Trainer.query();
        $scope.load = function(id) {
            CustomService.get({id : id}, function(result) {
                $scope.customService = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('bombproofbillingApp:customServiceUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.customService.id != null) {
                CustomService.update($scope.customService, onSaveFinished);
            } else {
                CustomService.save($scope.customService, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
