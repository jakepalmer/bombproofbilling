'use strict';

angular.module('bombproofbillingApp')
    .controller('CustomServiceController', function ($scope, CustomService) {
        $scope.customServices = [];
        $scope.loadAll = function() {
            CustomService.query(function(result) {
               $scope.customServices = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            CustomService.get({id: id}, function(result) {
                $scope.customService = result;
                $('#deleteCustomServiceConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            CustomService.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteCustomServiceConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.customService = {name: null, cost: null, id: null};
        };
    });
