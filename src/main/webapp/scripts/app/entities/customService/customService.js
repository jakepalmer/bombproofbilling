'use strict';

angular.module('bombproofbillingApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('customService', {
                parent: 'entity',
                url: '/customServices',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'CustomServices'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/customService/customServices.html',
                        controller: 'CustomServiceController'
                    }
                },
                resolve: {
                }
            })
            .state('customService.detail', {
                parent: 'entity',
                url: '/customService/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'CustomService'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/customService/customService-detail.html',
                        controller: 'CustomServiceDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'CustomService', function($stateParams, CustomService) {
                        return CustomService.get({id : $stateParams.id});
                    }]
                }
            })
            .state('customService.new', {
                parent: 'customService',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/customService/customService-dialog.html',
                        controller: 'CustomServiceDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {name: null, cost: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('customService', null, { reload: true });
                    }, function() {
                        $state.go('customService');
                    })
                }]
            })
            .state('customService.edit', {
                parent: 'customService',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/customService/customService-dialog.html',
                        controller: 'CustomServiceDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['CustomService', function(CustomService) {
                                return CustomService.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('customService', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
