'use strict';

angular.module('bombproofbillingApp')
    .controller('CustomServiceDetailController', function ($scope, $rootScope, $stateParams, entity, CustomService, Trainer) {
        $scope.customService = entity;
        $scope.load = function (id) {
            CustomService.get({id: id}, function(result) {
                $scope.customService = result;
            });
        };
        $rootScope.$on('bombproofbillingApp:customServiceUpdate', function(event, result) {
            $scope.customService = result;
        });
    });
