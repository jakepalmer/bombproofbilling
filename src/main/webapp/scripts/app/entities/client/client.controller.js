'use strict';

angular.module('bombproofbillingApp')
    .controller('ClientController', function ($scope, Client, Trainer, Auth) {
        $scope.trainers = [];
        $scope.data = {
            repeatSelect: null,
            availableOptions: [
                {id: '1', name: 'Client'}
            ],
            selectedOption: {id: '1', name: 'Client'}
        };
        $scope.emailConfirm = null;
        //Trainer info values
        $scope.userdetails = {
            email: null,
            login: null,
            langKey: 'en',
            firstName: null,
            lastName: null,
            roles: ['ROLE_USER']
        };

        $scope.client = {
            phone: null,
            firstName: null,
            lastName: null,
            address: null
        };

        $scope.horse = {
            name: '',
            usef: '',
            breed: ''
        };

        $scope.horses =[];

        $scope.addHorse = function() {
            $scope.horses.push({
                name: $scope.horse.name,
                usef: $scope.horse.usef,
                breed: $scope.horse.breed
            });
            $scope.horse = {};
            $scope.horse.name = null;
            $scope.horse.usef = null;
            $scope.horse.breed = null;
        };

        $scope.deleteFilteredItem = function(hashKey, sourceArray){
            angular.forEach(sourceArray, function(obj, index){
                // sourceArray is a reference to the original array passed to ng-repeat,
                // rather than the filtered version.
                // 1. compare the target object's hashKey to the current member of the iterable:
                if (obj.$$hashKey === hashKey) {
                    // remove the matching item from the array
                    sourceArray.splice(index, 1);
                    // and exit the loop right away
                    return;
                };
            });
        };
        $scope.createClient = function () {
            if ($scope.userdetails.email !== $scope.emailConfirm) {
                $scope.doNotMatch = 'ERROR';
            } else {
                $scope.userdetails.langKey =  'en' ;
                $scope.doNotMatch = null;
                $scope.error = null;
                $scope.errorUserExists = null;
                $scope.errorEmailExists = null;
                $scope.client = {
                    phone: $scope.client.phone.toString(),
                    firstName: $scope.userdetails.firstName,
                    lastName: $scope.userdetails.lastName,
                    address: $scope.client.address,
                    user: $scope.userdetails,
                    horses: $scope.horses,
                    trainer: $scope.trainers[0]
                };

                Auth.createClientAccount($scope.client).then(function (account) {
                    $scope.success = 'OK';
                    $('#modalTitle').text("Success")
                    $('#modalMessage').text("Successfully created Client " + $scope.userdetails.firstName + " " + $scope.userdetails.lastName);
                    $('#responseModal').modal('show');

                }).catch(function (response) {
                    $scope.success = null;
                    if (response.status === 400 && response.data === 'login already in use') {
                        $scope.errorUserExists = 'ERROR';
                        $('#modalTitle').text("Failed");
                        $('#modalMessage').text("Failed to create Client " + $scope.userdetails.firstName + " " + $scope.userdetails.lastName + ".  Login already in use.");
                        $('#responseModal').modal('show');
                    } else if (response.status === 400 && response.data === 'e-mail address already in use') {
                        $scope.errorEmailExists = 'ERROR';
                        $('#modalTitle').text("Failed");
                        $('#modalMessage').text("Failed to create Client " + $scope.userdetails.firstName + " " + $scope.userdetails.lastName + ".  E-Mail already in use.");
                        $('#responseModal').modal('show');
                    } else {
                        $scope.error = 'ERROR';
                        $('#modalTitle').text("Failed");
                        $('#modalMessage').text("Failed to create Client " + $scope.userdetails.firstName + " " + $scope.userdetails.lastName + ".");
                        $('#responseModal').modal('show');
                    }
                });
            }
        };

        $scope.loadAllTrainers = function() {
            Trainer.query(function(result) {
                $scope.trainers = result;
            });
        };
        $scope.loadAllTrainers();
        $scope.clear = function () {
            $scope.createClientForm.$setPristine();
            $scope.createClientForm.$setValidity();
            $scope.createClientForm.$setUntouched();
            $scope.client = {};
            $scope.horses = [];
            $scope.emailConfirm = null;
            $scope.userdetails = {
                email: null,
                login: null,
                langKey: 'en',
                firstName: null,
                lastName: null,
                roles: ['ROLE_USER']
            };
        };
    });
