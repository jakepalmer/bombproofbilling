'use strict';

angular.module('bombproofbillingApp').controller('ClientDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Admin', 'Client',
        function($scope, $stateParams, $modalInstance, entity, Admin, Client) {

        $scope.client = entity;
        $scope.load = function(id) {
            Client.get({id : id}, function(result) {
                $scope.client = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('bombproofbillingApp:clientUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.client.id != null) {
                Client.update($scope.client, onSaveFinished);
            } else {
                Client.save($scope.client, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
