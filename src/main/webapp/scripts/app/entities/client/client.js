'use strict';

angular.module('bombproofbillingApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('client', {
                parent: 'home',
                url: 'client',
                data: {
                    roles: ['ROLE_TRAINER', 'ROLE_ADMIN'],
                    pageTitle: 'Clients'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/client/clients.html',
                        controller: 'ClientController'
                    }
                },
                resolve: {
                }
            });
            //
            //.state('client.new', {
            //    parent: 'home',
            //    url: '/new',
            //    data: {
            //        roles: ['ROLE_TRAINER', 'ROLE_ADMIN'],
            //    },
            //    onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
            //        $modal.open({
            //            templateUrl: 'scripts/app/entities/client/client-dialog.html',
            //            controller: 'ClientDialogController',
            //            size: 'lg',
            //            resolve: {
            //                entity: function () {
            //                    return {clientId: null, firstName: null, lastName: null, address: null, phone: null, id: null};
            //                }
            //            }
            //        }).result.then(function(result) {
            //            $state.go('client', null, { reload: true });
            //        }, function() {
            //            $state.go('client');
            //        })
            //    }]
            //})

    });
