'use strict';

angular.module('bombproofbillingApp').controller('InvoiceDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Invoice', 'Client', 'Trainer', 'TrainerService', 'CustomService',
        function($scope, $stateParams, $modalInstance, entity, Invoice, Client, Trainer, TrainerService, CustomService) {

        $scope.invoice = entity;
        $scope.clients = Client.query();
        $scope.trainers = Trainer.query();
        $scope.trainerservices = TrainerService.query();
        $scope.customservices = CustomService.query();
        $scope.load = function(id) {
            Invoice.get({id : id}, function(result) {
                $scope.invoice = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('bombproofbillingApp:invoiceUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.invoice.id != null) {
                Invoice.update($scope.invoice, onSaveFinished);
            } else {
                Invoice.save($scope.invoice, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
