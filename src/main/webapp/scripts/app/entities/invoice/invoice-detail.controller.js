'use strict';

angular.module('bombproofbillingApp')
    .controller('InvoiceDetailController', function ($scope, $rootScope, $stateParams, entity, Invoice, Client, Trainer, TrainerService, CustomService) {
        $scope.invoice = entity;
        $scope.load = function (id) {
            Invoice.get({id: id}, function(result) {
                $scope.invoice = result;
            });
        };
        $rootScope.$on('bombproofbillingApp:invoiceUpdate', function(event, result) {
            $scope.invoice = result;
        });
    });
