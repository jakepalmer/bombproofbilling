'use strict';

angular.module('bombproofbillingApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('invoice', {
                parent: 'entity',
                url: '/invoices',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Invoices'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/invoice/invoices.html',
                        controller: 'InvoiceController'
                    }
                },
                resolve: {
                }
            })
            .state('invoice.detail', {
                parent: 'entity',
                url: '/invoice/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Invoice'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/invoice/invoice-detail.html',
                        controller: 'InvoiceDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Invoice', function($stateParams, Invoice) {
                        return Invoice.get({id : $stateParams.id});
                    }]
                }
            })
            .state('invoice.new', {
                parent: 'invoice',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/invoice/invoice-dialog.html',
                        controller: 'InvoiceDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {date: null, total: null, paid: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('invoice', null, { reload: true });
                    }, function() {
                        $state.go('invoice');
                    })
                }]
            })
            .state('invoice.edit', {
                parent: 'invoice',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/invoice/invoice-dialog.html',
                        controller: 'InvoiceDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Invoice', function(Invoice) {
                                return Invoice.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('invoice', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })

//        Begin the admin invoice section
        .state('newinvoices', {
            parent: 'home',
            url: 'invoices/recent',
            data: {
                roles: ['ROLE_ADMIN']
            },
            views: {
                'content@': {
                    templateUrl: 'scripts/app/entities/invoice/invoices.html',
                    controller: 'InvoiceController'
                }
            }
        })
        .state('newinvoice', {
            parent: 'home',
            url: 'invoices/newinvoice',
            data: {
                roles: ['ROLE_TRAINER']
            },
            views: {
                'content@': {
                    templateUrl: 'scripts/app/entities/invoice/newinvoice.html',
                    controller: 'NewInvoiceController'
                }
            }
        })
        .state('unpaidinvoices', {
            parent: 'home',
            url: 'invoices/unpaid',
            data: {
                roles: ['ROLE_ADMIN']
            },
            views: {
                'content@': {
                    templateUrl: 'scripts/app/entities/invoice/unpaidinvoices.html',
                    controller: 'InvoiceController'
                }
            }
        })
        .state('readyforpayment', {
            parent: 'home',
            url: 'invoices/pay',
            data: {
                roles: ['ROLE_ADMIN']
            },
            views: {
                'content@': {
                    templateUrl: 'scripts/app/entities/invoice/readyinvoices.html',
                    controller: 'InvoiceController'
                }
            }
        });
    });
