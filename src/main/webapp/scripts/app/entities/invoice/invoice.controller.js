'use strict';

angular.module('bombproofbillingApp')
    .controller('InvoiceController', function ($scope, Invoice) {
        $scope.invoices = [];
        $scope.loadAll = function() {
            Invoice.query(function(result) {
               $scope.invoices = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Invoice.get({id: id}, function(result) {
                $scope.invoice = result;
                $('#deleteInvoiceConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Invoice.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteInvoiceConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.invoice = {date: null, total: null, paid: null, id: null};
        };
    });
