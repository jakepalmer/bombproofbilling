'use strict';

angular.module('bombproofbillingApp')
    .controller('NewInvoiceController', function ($scope, Invoice, Trainer, Principal, $modal, $rootScope) {
        $scope.clients = [];
        $scope.items = [];
        $scope.addedServices = [];
        $scope.totalCost = 0;
        $scope.loadAll = function() {
            Principal.identity().then(function(account) {
                Trainer.getByLogin({login: account.login}, function(result){
                    $scope.trainer = result;
//                    $scope.trainerServices = result.trainerService;
                });
            });

        };
        $scope.loadAll();

        $('#datepicker').datepicker();
        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.addServiceToList = function(service) {
            $scope.addedServices.push(service);
            $scope.totalCost = $scope.totalCost + service.cost;
            $scope.clear();
        };

        $scope.clearItems = function() {
            $scope.addedServices = [];
            $scope.totalCost = 0;
        };


        //Setup UI Bootstrap Modal
//        var modalScope = $rootScope.$new();
        var openModal = function() {
            $scope.controller = this;
            $scope.modalInstance =  $modal.open({
                templateUrl: 'scripts/app/entities/invoice/new-invoice-modal.html',
                size: 'lg',
                scope: $scope,
                controller: $scope.controller

            });
        };
        $scope.openNewInvoiceModal = function() {
            openModal();
        };

        $scope.clear = function() {
            $scope.modalInstance.dismiss('cancel');
        }
    });
