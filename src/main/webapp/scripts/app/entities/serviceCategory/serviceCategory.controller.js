'use strict';

angular.module('bombproofbillingApp')
    .controller('ServiceCategoryController', function ($scope, ServiceCategory) {
        $scope.serviceCategorys = [];
        $scope.loadAll = function() {
            ServiceCategory.query(function(result) {
               $scope.serviceCategorys = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            ServiceCategory.get({id: id}, function(result) {
                $scope.serviceCategory = result;
                $('#deleteServiceCategoryConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            ServiceCategory.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteServiceCategoryConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.serviceCategory = {name: null, id: null};
        };
    });
