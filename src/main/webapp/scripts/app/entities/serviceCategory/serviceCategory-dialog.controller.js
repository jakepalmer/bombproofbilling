'use strict';

angular.module('bombproofbillingApp').controller('ServiceCategoryDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ServiceCategory', 'Service',
        function($scope, $stateParams, $modalInstance, entity, ServiceCategory, Service) {

        $scope.serviceCategory = entity;
        $scope.services = Service.query();
        $scope.load = function(id) {
            ServiceCategory.get({id : id}, function(result) {
                $scope.serviceCategory = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('bombproofbillingApp:serviceCategoryUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.serviceCategory.id != null) {
                ServiceCategory.update($scope.serviceCategory, onSaveFinished);
            } else {
                ServiceCategory.save($scope.serviceCategory, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
