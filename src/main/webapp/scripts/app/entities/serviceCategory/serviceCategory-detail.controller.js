'use strict';

angular.module('bombproofbillingApp')
    .controller('ServiceCategoryDetailController', function ($scope, $rootScope, $stateParams, entity, ServiceCategory, Service) {
        $scope.serviceCategory = entity;
        $scope.load = function (id) {
            ServiceCategory.get({id: id}, function(result) {
                $scope.serviceCategory = result;
            });
        };
        $rootScope.$on('bombproofbillingApp:serviceCategoryUpdate', function(event, result) {
            $scope.serviceCategory = result;
        });
    });
