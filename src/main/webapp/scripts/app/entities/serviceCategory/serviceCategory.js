'use strict';

angular.module('bombproofbillingApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('serviceCategory', {
                parent: 'entity',
                url: '/serviceCategorys',
                data: {
                    roles: ['ROLE_CLIENT'],
                    pageTitle: 'ServiceCategorys'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/serviceCategory/serviceCategorys.html',
                        controller: 'ServiceCategoryController'
                    }
                },
                resolve: {
                }
            })
            .state('serviceCategory.detail', {
                parent: 'entity',
                url: '/serviceCategory/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'ServiceCategory'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/serviceCategory/serviceCategory-detail.html',
                        controller: 'ServiceCategoryDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'ServiceCategory', function($stateParams, ServiceCategory) {
                        return ServiceCategory.get({id : $stateParams.id});
                    }]
                }
            })
            .state('serviceCategory.new', {
                parent: 'serviceCategory',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/serviceCategory/serviceCategory-dialog.html',
                        controller: 'ServiceCategoryDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {name: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('serviceCategory', null, { reload: true });
                    }, function() {
                        $state.go('serviceCategory');
                    })
                }]
            })
            .state('serviceCategory.edit', {
                parent: 'serviceCategory',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/serviceCategory/serviceCategory-dialog.html',
                        controller: 'ServiceCategoryDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ServiceCategory', function(ServiceCategory) {
                                return ServiceCategory.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('serviceCategory', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
