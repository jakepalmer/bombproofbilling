'use strict';

angular.module('bombproofbillingApp').controller('TrainerClientAssignController',
        function($scope, $stateParams, Admin, Trainer, Client, $modal, $rootScope) {

            $scope.trainer = {};
            $scope.outputClients = [];
            $scope.availableClients = [];
            $scope.allClients = [];

            $scope.load = function(id) {
                $scope.allClients = Client.query(function(){
                    Trainer.get({id : id}, function(result) {
                        $scope.trainer = result;
                        if ($scope.trainer.client.length == 0) {
                            angular.copy($scope.allClients, $scope.availableClients);
                        } else {
                            for (var x = 0; x < $scope.allClients.length; x++) {
                                var isInArray = false;
                                for (var i = 0; i < $scope.trainer.client.length; i++) {
                                    if ($scope.trainer.client[i].id == $scope.allClients[x].id) {
                                        isInArray = true;
                                    }
                                }
                                if (isInArray == false) {
                                    $scope.availableClients.push($scope.allClients[x]);
                                } else {
                                    isInArray = false;
                                }
                            }
                        }
                    });
                });

            };
            //Select Grids
            // init
            $scope.selectedA = [];
            $scope.selectedB = [];


            $scope.checkedA = false;
            $scope.checkedB = false;

            function arrayObjectIndexOf(myArray, searchTerm, property) {
                for(var i = 0, len = myArray.length; i < len; i++) {
                    if (myArray[i][property] === searchTerm) return i;
                }
                return -1;
            }

            $scope.aToB = function() {
                for (var i in $scope.selectedA) {
                    var moveId = arrayObjectIndexOf($scope.allClients, $scope.selectedA[i], "id");
                    $scope.trainer.client.push($scope.allClients[moveId]);
                    var delId = arrayObjectIndexOf($scope.availableClients, $scope.selectedA[i], "id");
                    $scope.availableClients.splice(delId,1);
                }
                reset();
            };

            $scope.bToA = function() {
                for (var i in $scope.selectedB) {
                    var moveId = arrayObjectIndexOf($scope.allClients, $scope.selectedB[i], "id");
                    $scope.availableClients.push($scope.allClients[moveId]);
                    var delId = arrayObjectIndexOf($scope.trainer.client, $scope.selectedB[i], "id");
                    $scope.trainer.client.splice(delId,1);
                }
                reset();
            };

            function reset(){
                $scope.selectedA=[];
                $scope.selectedB=[];
                $scope.toggleAAll=0;
                $scope.toggleBAll=0;
            }

            $scope.toggleA = function() {
                if ($scope.selectedA.length>0) {
                    $scope.selectedA=[];
                }
                else {
                    for (var i in $scope.availableClients) {
                        $scope.selectedA.push($scope.availableClients[i].id);
                    }
                }
            };

            $scope.toggleB = function() {
                if ($scope.selectedB.length>0) {
                    $scope.selectedB=[];
                }
                else {
                    for (var i in $scope.trainer.client) {
                        $scope.selectedB.push($scope.trainer.client[i].id);
                    }
                }
            };

            $scope.selectA = function(i) {
                $scope.selectedA.push(i);
            };

            $scope.selectB = function(i) {
                $scope.selectedB.push(i);
            };

            $scope.load($stateParams.id);

            var onSaveFinished = function (result) {
                $scope.$emit('bombproofbillingApp:clientUpdate', result);
            };

            $scope.saveTrainer = function () {
                if ($scope.trainer.id != null) {
                    var clientIds = [];
                    for (var i in $scope.trainer.client) {
                        clientIds.push($scope.trainer.client[i].id);
                    }
                    Trainer.updateClients($scope.trainer, function(result) {
                        $('#modalTitle').text("Success");
                        $('#modalMessage').text("Successfully modified " + $scope.trainer.firstName + " " + $scope.trainer.lastName + "'s Client List.");
                        $('#responseModal').modal('show');
                    });
                    //}).catch(function (response) {
                    //        $scope.success = null;
                    //        if (response.status === 400 || response.status === 500) {
                    //            $('#modalTitle').text("Failed");
                    //            $('#modalMessage').text("Failed to modify " + $scope.trainer.firstName + " " + $scope.trainer.lastName + "'s Client List.");
                    //            $('#responseModal').modal('show');
                    //        }
                    //});
                }
            };



            //Setup UI Bootstrap Modal
            var modalScope = $rootScope.$new();
            var openModal = function(client) {
                modalScope.modalInstance =  $modal.open({
                    templateUrl: 'scripts/app/entities/trainer/trainer-client-detail.html',
                    size: 'lg',
                    scope: modalScope,
                    controller: 'TrainerClientDetailController',
                    resolve: {
                        entity: function() { return Client.get({id: client})}
                    }

                });
            };

            $scope.client = function(userId) {
                return Client.get({id: userId});
            };
            $scope.openClientDetailsModal = function(userId) {
                    openModal(userId);
            };

            $scope.clear = function() {
                $scope.modalInstance.dismiss('cancel');
            }
        });
