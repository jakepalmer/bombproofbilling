'use strict';

angular.module('bombproofbillingApp')
    .controller('TrainerServicesController', function ($scope, $rootScope, $stateParams, entity, Trainer, Service) {
        $scope.trainer = entity;
        $scope.load = function (id) {
            Service.query(function(result) {
                $scope.services = result;
                $scope.availableServices = [];
                for (var i = 0; i < $scope.services.length; i++) {
                    var serviceName = $scope.services[i].name;
                    var serviceCategory = $scope.services[i].serviceCategory.name;
                    if ($scope.trainer.trainerService == null) {
                        $scope.trainer.trainerService = [];
                    }
                    if ($scope.services[i].name != null) {
                        $scope.availableServices.push($scope.services[i]);
                    }
                    for (var x = 0; x < $scope.trainer.trainerService; x++) {
                        if (!$scope.trainer.trainerService.name == serviceName &&
                            !$scope.trainer.trainerService.serviceCategory.name == serviceCategory) {
                            $scope.availableServices.splice($scope.services[i], 1);
                            break;
                        }
                    }

                }
            });
        };

        $scope.load($scope.trainer.id);

        $rootScope.$on('bombproofbillingApp:trainerUpdate', function(event, result) {
            $scope.trainer = result;
        });

        $scope.saveTrainerServices = function () {
            if ($scope.trainer.id != null) {
                var serviceIds = [];
                for (var i in $scope.trainer.trainerService) {
                    serviceIds.push($scope.trainer.trainerService[i].id);
                }
                //Trainer.updateServices($scope.trainer, function(result) {
                //    $('#modalTitle').text("Success");
                //    $('#modalMessage').text("Successfully modified " + $scope.trainer.firstName + " " + $scope.trainer.lastName + "'s Client List.");
                //    $('#responseModal').modal('show');
                //});
                //}).catch(function (response) {
                //        $scope.success = null;
                //        if (response.status === 400 || response.status === 500) {
                //            $('#modalTitle').text("Failed");
                //            $('#modalMessage').text("Failed to modify " + $scope.trainer.firstName + " " + $scope.trainer.lastName + "'s Client List.");
                //            $('#responseModal').modal('show');
                //        }
                //});
            }
        };


        //Select Grids
        // init
        $scope.selectedA = [];
        $scope.selectedB = [];


        $scope.checkedA = false;
        $scope.checkedB = false;

        function arrayObjectIndexOf(myArray, searchTerm, property) {
            for(var i = 0, len = myArray.length; i < len; i++) {
                if (myArray[i][property] === searchTerm) return i;
            }
            return -1;
        }

        $scope.aToB = function() {
            for (var i in $scope.selectedA) {
                var moveId = arrayObjectIndexOf($scope.availableServices, $scope.selectedA[i], "id");
                $scope.trainer.trainerService.push($scope.availableServices[moveId]);
                var delId = arrayObjectIndexOf($scope.availableServices, $scope.selectedA[i], "id");
                $scope.availableServices.splice(delId,1);
            }
            reset();
        };

        $scope.bToA = function() {
            for (var i in $scope.selectedB) {
                var moveId = arrayObjectIndexOf($scope.trainer.trainerService, $scope.selectedB[i], "id");
                $scope.availableServices.push($scope.trainer.trainerService[moveId]);
                var delId = arrayObjectIndexOf($scope.trainer.trainerService, $scope.selectedB[i], "id");
                $scope.trainer.trainerService.splice(delId,1);
            }
            reset();
        };

        function reset(){
            $scope.selectedA=[];
            $scope.selectedB=[];
            $scope.toggleAAll=0;
            $scope.toggleBAll=0;
        }

        $scope.toggleA = function() {
            if ($scope.selectedA.length>0) {
                $scope.selectedA=[];
            }
            else {
                for (var i in $scope.availableServices) {
                    $scope.selectedA.push($scope.availableServices[i].id);
                    console.log($scope.availableServices[i]);
                }
            }
        };

        $scope.toggleB = function() {
            if ($scope.selectedB.length>0) {
                $scope.selectedB=[];
            }
            else {
                for (var i in $scope.trainer.trainerService) {
                    $scope.selectedB.push($scope.trainer.trainerService[i].id);
                }
            }
        };

        $scope.selectA = function(i) {
            $scope.selectedA.push(i);
        };

        $scope.selectB = function(i) {
            $scope.selectedB.push(i);
        };


    });
