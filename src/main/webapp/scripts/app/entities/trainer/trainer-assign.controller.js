'use strict';

angular.module('bombproofbillingApp').controller('TrainerAssignController',
    ['$scope', '$stateParams', 'trainers', 'Trainer', 'User',
        function($scope, $stateParams, trainers, Trainer, User) {

            $scope.trainers = trainers;
            $scope.trainer = {};
            $scope.users = User.query();

            var onSaveFinished = function (result) {
                $scope.$emit('bombproofbillingApp:trainerUpdate', result);
            };

            $scope.save = function () {
                if ($scope.trainer.id != null) {
                    Trainer.update($scope.trainer, onSaveFinished);
                } else {
                    Trainer.save($scope.trainer, onSaveFinished);
                }
            };
        }]);
