'use strict';

angular.module('bombproofbillingApp')
    .controller('TrainerController', function ($scope, Auth, Trainer) {
        $scope.trainers = [];
        $scope.data = {
            repeatSelect: null,
            availableOptions: [
                {id: '1', name: 'Trainer'}
            ],
            selectedOption: {id: '1', name: 'Trainer'}
        };
        $scope.emailConfirm = null;
        //Trainer info values
        $scope.userdetails = {
            email: null,
            login: null,
            langKey: 'en',
            firstName: null,
            lastName: null,
            roles: ['ROLE_TRAINER']
        };

        $scope.trainer = {
            phone: null,
            firstName: null,
            lastName: null,
            address: null
        }

        $scope.createTrainer = function () {
            if ($scope.userdetails.email !== $scope.emailConfirm) {
                $scope.doNotMatch = 'ERROR';
            } else {
                $scope.userdetails.langKey =  'en' ;
                $scope.doNotMatch = null;
                $scope.error = null;
                $scope.errorUserExists = null;
                $scope.errorEmailExists = null;

                $scope.trainer = {
                    phone: $scope.trainer.phone,
                    firstName: $scope.userdetails.firstName,
                    lastName: $scope.userdetails.lastName,
                    address: $scope.trainer.address,
                    user: $scope.userdetails

                };

                Auth.createTrainerAccount($scope.trainer).then(function (account) {
                    $scope.success = 'OK';
                    $('#modalTitle').text("Success")
                    $('#modalMessage').text("Successfully created Trainer " + $scope.userdetails.firstName + " " + $scope.userdetails.lastName);
                    $('#responseModal').modal('show');

                }).catch(function (response) {
                    $scope.success = null;
                    if (response.status === 400 && response.data === 'login already in use') {
                        $scope.errorUserExists = 'ERROR';
                        $('#modalTitle').text("Failed")
                        $('#modalMessage').text("Failed to create Trainer " + $scope.userdetails.firstName + " " + $scope.userdetails.lastName + ".  Login already in use.");
                        $('#responseModal').modal('show');
                    } else if (response.status === 400 && response.data === 'e-mail address already in use') {
                        $scope.errorEmailExists = 'ERROR';
                        $('#modalTitle').text("Failed")
                        $('#modalMessage').text("Failed to create Trainer " + $scope.userdetails.firstName + " " + $scope.userdetails.lastName + ".  E-Mail already in use.");
                        $('#responseModal').modal('show');
                    } else {
                        $scope.error = 'ERROR';
                        $('#modalTitle').text("Failed")
                        $('#modalMessage').text("Failed to create Trainer " + $scope.userdetails.firstName + " " + $scope.userdetails.lastName + ".");
                        $('#responseModal').modal('show');
                    }
                });
            }
        };
        $scope.loadAll = function() {
            Trainer.query(function(result) {
               $scope.trainers = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Trainer.get({id: id}, function(result) {
                $scope.trainer = result;
                $('#deleteTrainerConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Trainer.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTrainerConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.createTrainerForm.$setPristine();
            $scope.createTrainerForm.$setValidity();
            $scope.createTrainerForm.$setUntouched();
            $scope.trainer = {};
            $scope.emailConfirm = null;
            $scope.userdetails = {
                email: null,
                login: null,
                langKey: 'en',
                firstName: null,
                lastName: null,
                roles: ['ROLE_TRAINER']
            };
        };
    });
