'use strict';

angular.module('bombproofbillingApp')
    .controller('TrainerClientDetailController', function ($scope, $rootScope, $modalInstance ,$stateParams, entity, Client) {
        $scope.client = entity;
        $scope.load = function (id) {
            Client.get({id: id}, function(result) {
                $scope.client = result;
            });
        };

        $scope.previousState =  $rootScope.previousStateName;
        $rootScope.$on('bombproofbillingApp:clientUpdate', function(event, result) {
            $scope.client = result;
        });
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
    });
