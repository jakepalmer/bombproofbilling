'use strict';

angular.module('bombproofbillingApp')
    .controller('TrainerMyServicesController', function ($scope, $rootScope, $stateParams, Trainer, Principal, Service) {
        $scope.trainer = {};
        $scope.load = function () {
            //Load the trainer specific information
                Principal.identity().then(function(account) {
                    Trainer.getByLogin({login: account.login}, function (data) {
                        $scope.trainer = data;
                            Service.query(function(result) {
                                $scope.services = result;
                                $scope.availableServices = [];
                                for (var i = 0; i < $scope.services.length; i++) {
                                    var serviceName = $scope.services[i].name;
                                    var serviceCategory = $scope.services[i].serviceCategory.name;
                                    if ($scope.services[i].name != null) {
                                        $scope.availableServices.push($scope.services[i]);
                                    }

                                    //Remove all trainer services from available services
                                    for (var x = 0; x < $scope.trainer.trainerService.length; x++) {
                                        var trainerServiceName = $scope.trainer.trainerService[x].service.name;
                                        var serviceCategoryName = $scope.trainer.trainerService[x].service.serviceCategory.name;
                                        if (serviceCategoryName == undefined || serviceCategoryName == "" || serviceCategoryName == null) {
                                            serviceCategoryName = $scope.trainer.trainerService[x].service.service.serviceCategory.name;
                                        }
                                        if (trainerServiceName == serviceName &&
                                            serviceCategoryName == serviceCategory) {
                                            var delId = arrayObjectIndexOfAdvanced($scope.availableServices, $scope.services[i].name, "name", $scope.services[i].serviceCategory.name, "name");
                                            $scope.availableServices.splice(delId, 1);
                                        }
                                    }

                                }
                            });
                        });
                    });
        };

        $scope.load();

        $rootScope.$on('bombproofbillingApp:trainerUpdate', function(event, result) {
            $scope.trainer = result;
        });

        $scope.addService = function(id, cost) {
            if (cost != null && cost != undefined && cost != "") {
                var moveId = arrayObjectIndexOf($scope.availableServices, id, "id");
                $scope.trainer.trainerService.push($scope.availableServices[moveId]);
                var delId = arrayObjectIndexOf($scope.availableServices, id, "id");
                $scope.availableServices.splice(delId, 1);
                $scope.saveTrainerServices(id);
            } else {
                $('#modalTitle').text("Error");
                $('#modalMessage').text("Please assign a cost value.");
                $('#responseModal').modal('show');
            }
        };

        $scope.removeService = function(id) {
            var moveId = arrayObjectIndexOf($scope.trainer.trainerService, id, "id");
            $scope.availableServices.push($scope.trainer.trainerService[moveId]);
            var delId = arrayObjectIndexOf($scope.trainer.trainerService, id, "id");
            $scope.trainer.trainerService.splice(delId,1);
            $scope.saveTrainerServices(id);
        };

        function arrayObjectIndexOf(myArray, searchTerm, property) {
            for(var i = 0, len = myArray.length; i < len; i++) {
                if (myArray[i][property] === searchTerm) return i;
            }
            return -1;
        }
        function arrayObjectIndexOfAdvanced(myArray, searchTerm, property, searchTerm2, property2) {
            for(var i = 0, len = myArray.length; i < len; i++) {
                if (myArray[i][property] === searchTerm &&
                    myArray[i][property2] == searchTerm2) return i;
            }
            return -1;
        }

        $scope.saveTrainerServices = function (id) {
            if ($scope.trainer.id != null) {
                var serviceIds = [];
                for (var i in $scope.trainer.trainerService) {
                    serviceIds.push($scope.trainer.trainerService[i].id);
                }
                Trainer.updateServices($scope.trainer, function(result) {
                    $('#modalTitle').text("Success");
                    $('#modalMessage').text("Successfully modified your services.");
                    $('#responseModal').modal('show');
                    $scope.load();
                }, function(response) {
                    if (response.status === 400 || response.status === 500) {
                        $('#modalTitle').text("Failed");
                        $('#modalMessage').text("Failed to modify your services.");
                        $('#responseModal').modal('show');
                        var moveId = arrayObjectIndexOf($scope.trainer.trainerService, id, "id");
                        $scope.availableServices.push($scope.trainer.trainerService[moveId]);
                        var delId = arrayObjectIndexOf($scope.trainer.trainerService, id, "id");
                        $scope.trainer.trainerService.splice(delId,1);
                    }
                });
            }
        };




    });
