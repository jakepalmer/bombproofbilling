'use strict';

angular.module('bombproofbillingApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('trainer', {
                parent: 'home',
                url: 'trainers',
                data: {
                    roles: ['ROLE_ADMIN'],
                    pageTitle: 'Trainers'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/trainer/trainers.html',
                        controller: 'TrainerController'
                    }
                },
                resolve: {
                }
            })

            .state('trainer.trainers', {
                parent: 'home',
                url: 'all_trainers',
                data: {
                    roles: ['ROLE_ADMIN']
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/trainer/trainer-assign.html',
                        controller: 'TrainerAssignController'
                    }
                },
                resolve: {
                    trainers: ['Trainer', function(Trainer) {
                        return Trainer.query();
                    }]
                }
            })

            .state('trainer.editclients', {
                parent: 'trainer.trainers',
                url: '/editclients/{id}',
                data: {
                    roles: ['ROLE_ADMIN', 'ROLE_TRAINER']
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/trainer/trainer-client-assign.html',
                        controller: 'TrainerClientAssignController'
                    }
                }
            })
            .state('trainer.editmyclients', {
                parent: 'trainer.trainers',
                url: '/editmyclients',
                data: {
                    roles: ['ROLE_TRAINER']
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/trainer/trainer-myclient-assign.html',
                        controller: 'TrainerMyClientAssignController'
                    }
                }
            })

            .state('trainer.services', {
                parent: 'trainer.trainers',
                url: '/services/{id}',
                data: {
                    roles: ['ROLE_ADMIN']
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/trainer/trainer-services.html',
                        controller: 'TrainerServicesController'
                    }
                },
                resolve: {
                    entity: ['Trainer', '$stateParams', function(Trainer, $stateParams) {
                        return Trainer.get({id: $stateParams.id});
                    }]
                }
            })

            .state('trainer.services.assign', {
                parent: 'trainer.trainers',
                url: '/services/assign/{id}',
                data: {
                    roles: ['ROLE_ADMIN']
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/trainer/trainer-service-assign.html',
                        controller: 'TrainerServicesController'
                    }
                },
                resolve: {
                    entity: ['Trainer', '$stateParams', function(Trainer, $stateParams) {
                        return Trainer.get({id: $stateParams.id});
                    }]
                }
            })

            .state('trainer.myservices', {
                parent: 'home',
                url: 'myservices',
                data: {
                    roles: ['ROLE_TRAINER']
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/trainer/trainer-myservice-assign.html',
                        controller: 'TrainerMyServicesController'
                    }
                }
            });

    });
