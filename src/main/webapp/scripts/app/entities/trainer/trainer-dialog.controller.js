'use strict';

angular.module('bombproofbillingApp').controller('TrainerDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Trainer', 'User',
        function($scope, $stateParams, $modalInstance, entity, Trainer, User) {

        $scope.trainer = entity;
        $scope.users = User.query();
        $scope.load = function(id) {
            Trainer.get({id : id}, function(result) {
                $scope.trainer = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('bombproofbillingApp:trainerUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.trainer.id != null) {
                Trainer.update($scope.trainer, onSaveFinished);
            } else {
                Trainer.save($scope.trainer, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
