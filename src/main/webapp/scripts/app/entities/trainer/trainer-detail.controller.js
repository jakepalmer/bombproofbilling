'use strict';

angular.module('bombproofbillingApp')
    .controller('TrainerDetailController', function ($scope, $rootScope,$modalInstance, $stateParams, entity, Trainer, User) {
        $scope.trainer = entity;
        $scope.load = function (id) {
            Trainer.get({id: id}, function(result) {
                $scope.trainer = result;
            });
        };
        $rootScope.$on('bombproofbillingApp:trainerUpdate', function(event, result) {
            $scope.trainer = result;
        });
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
    });
