'use strict';

angular.module('bombproofbillingApp')
    .controller('AdminDetailController', function ($scope, $rootScope, $stateParams, entity, Client, $modalInstance) {
        $scope.client = entity;
        $scope.load = function (id) {
            Client.get({id: id}, function(result) {
                $scope.client = result;
            });
        };
        $rootScope.$on('bombproofbillingApp:clientUpdate', function(event, result) {
            $scope.client = result;
        });
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
    });
