'use strict';

angular.module('bombproofbillingApp').controller('AdminDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Admin',
        function($scope, $stateParams, $modalInstance, entity, Admin) {

        $scope.client = entity;
        $scope.load = function(id) {
            Admin.get({id : id}, function(result) {
                $scope.client = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('bombproofbillingApp:clientUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.client.id != null) {
                Admin.update($scope.client, onSaveFinished);
            } else {
                Admin.save($scope.client, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
