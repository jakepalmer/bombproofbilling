'use strict';

angular.module('bombproofbillingApp')
    .controller('AdminController', function ($scope, Client, Auth) {
        $scope.data = {
            repeatSelect: null,
            availableOptions: [
                {id: '1', name: 'Admin'}
            ],
            selectedOption: {id: '1', name: 'Admin'}
        };
        $scope.emailConfirm = null;

        //Trainer info values
        $scope.userdetails = {
            email: null,
            login: null,
            langKey: 'en',
            firstName: null,
            lastName: null,
            roles: ['ROLE_ADMIN']
        };


        $scope.createAdmin = function () {
            if ($scope.userdetails.email !== $scope.emailConfirm) {
                $scope.doNotMatch = 'ERROR';
            } else {
                $scope.userdetails.langKey =  'en' ;
                $scope.doNotMatch = null;
                $scope.error = null;
                $scope.errorUserExists = null;
                $scope.errorEmailExists = null;


                Auth.createAdminAccount($scope.userdetails).then(function (account) {
                    $scope.success = 'OK';
                    $('#modalTitle').text("Success")
                    $('#modalMessage').text("Successfully created Admin " + $scope.userdetails.firstName + " " + $scope.userdetails.lastName);
                    $('#responseModal').modal('show');

                }).catch(function (response) {
                    $scope.success = null;
                    if (response.status === 400 && response.data === 'login already in use') {
                        $scope.errorUserExists = 'ERROR';
                        $('#modalTitle').text("Failed")
                        $('#modalMessage').text("Failed to create Admin " + $scope.userdetails.firstName + " " + $scope.userdetails.lastName + ".  Login already in use.");
                        $('#responseModal').modal('show');
                    } else if (response.status === 400 && response.data === 'e-mail address already in use') {
                        $scope.errorEmailExists = 'ERROR';
                        $('#modalTitle').text("Failed")
                        $('#modalMessage').text("Failed to create Admin " + $scope.userdetails.firstName + " " + $scope.userdetails.lastName + ".  E-Mail already in use.");
                        $('#responseModal').modal('show');
                    } else {
                        $scope.error = 'ERROR';
                        $('#modalTitle').text("Failed")
                        $('#modalMessage').text("Failed to create Admin " + $scope.userdetails.firstName + " " + $scope.userdetails.lastName + ".");
                        $('#responseModal').modal('show');
                    }
                });
            }
        };

        $scope.clear = function () {
            $scope.createAdminForm.$setPristine();
            $scope.createAdminForm.$setValidity();
            $scope.createAdminForm.$setUntouched();
            $scope.emailConfirm = null;
            $scope.userdetails = {
                email: null,
                login: null,
                langKey: 'en',
                firstName: null,
                lastName: null,
                roles: ['ROLE_ADMIN']
            };
        };
    });
