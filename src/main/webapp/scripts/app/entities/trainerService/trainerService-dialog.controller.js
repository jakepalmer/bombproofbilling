'use strict';

angular.module('bombproofbillingApp').controller('TrainerServiceDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'TrainerService', 'Service', 'Trainer',
        function($scope, $stateParams, $modalInstance, entity, TrainerService, Service, Trainer) {

        $scope.trainerService = entity;
        $scope.services = Service.query();
        $scope.trainers = Trainer.query();
        $scope.load = function(id) {
            TrainerService.get({id : id}, function(result) {
                $scope.trainerService = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('bombproofbillingApp:trainerServiceUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.trainerService.id != null) {
                TrainerService.update($scope.trainerService, onSaveFinished);
            } else {
                TrainerService.save($scope.trainerService, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
