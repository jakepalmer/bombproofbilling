'use strict';

angular.module('bombproofbillingApp')
    .controller('TrainerServiceDetailController', function ($scope, $rootScope, $stateParams, entity, TrainerService, Service, Trainer) {
        $scope.trainerService = entity;
        $scope.load = function (id) {
            TrainerService.get({id: id}, function(result) {
                $scope.trainerService = result;
            });
        };
        $rootScope.$on('bombproofbillingApp:trainerServiceUpdate', function(event, result) {
            $scope.trainerService = result;
        });
    });
