'use strict';

angular.module('bombproofbillingApp')
    .controller('TrainerServiceController', function ($scope, TrainerService) {
        $scope.trainerServices = [];
        $scope.loadAll = function() {
            TrainerService.query(function(result) {
               $scope.trainerServices = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            TrainerService.get({id: id}, function(result) {
                $scope.trainerService = result;
                $('#deleteTrainerServiceConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            TrainerService.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTrainerServiceConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.trainerService = {name: null, cost: null, id: null};
        };
    });
