'use strict';

angular.module('bombproofbillingApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('trainerService', {
                parent: 'entity',
                url: '/trainerServices',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'TrainerServices'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/trainerService/trainerServices.html',
                        controller: 'TrainerServiceController'
                    }
                },
                resolve: {
                }
            })
            .state('trainerService.detail', {
                parent: 'entity',
                url: '/trainerService/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'TrainerService'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/trainerService/trainerService-detail.html',
                        controller: 'TrainerServiceDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'TrainerService', function($stateParams, TrainerService) {
                        return TrainerService.get({id : $stateParams.id});
                    }]
                }
            })
            .state('trainerService.new', {
                parent: 'trainerService',
                url: '/new',
                data: {
                    roles: ['ROLE_TRAINER']
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/trainerService/trainerService-dialog.html',
                        controller: 'TrainerServiceDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {name: null, cost: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('trainerService', null, { reload: true });
                    }, function() {
                        $state.go('trainerService');
                    })
                }]
            })
            .state('trainerService.edit', {
                parent: 'trainerService',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/trainerService/trainerService-dialog.html',
                        controller: 'TrainerServiceDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['TrainerService', function(TrainerService) {
                                return TrainerService.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('trainerService', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
