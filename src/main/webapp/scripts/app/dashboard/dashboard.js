'use strict';

angular.module('bombproofbillingApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('home', {
                parent: 'site',
                url: '/',
                data: {
                    roles: ['ROLE_ADMIN', 'ROLE_TRAINER']
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/dashboard/dashboard.html',
                        controller: 'DashboardController'
                    },
                    'navbar@': {
                        templateUrl: 'scripts/components/navbar/nav.html',
                        controller: 'NavbarController'
                    },
                    'header@': {
                        templateUrl: 'scripts/app/header/header.html',
                        controller: 'HeaderController'
                    }
                },
                resolve: {
                    authorize: ['Auth',
                        function (Auth) {
                            return Auth.authorize();
                        }
                    ]
                }
            })
    });
