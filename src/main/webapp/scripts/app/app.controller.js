/**
 * Created by jake on 9/12/15.
 */
'use strict';

angular.module("bombproofbillingApp")
    .controller("AppController", function($rootScope, $scope, $timeout, Principal) {

        $scope.isAuthenticated = Principal.isAuthenticated;
    var mm = window.matchMedia("(max-width: 767px)");
    $rootScope.isMobile = mm.matches ? true: false;

    $rootScope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    mm.addListener(function(m) {
        $rootScope.safeApply(function() {
            $rootScope.isMobile = (m.matches) ? true : false;
        });
    });


    $scope.navFull = true;
    $scope.toggleNav = function() {
        $scope.navFull = $scope.navFull ? false : true;
        $rootScope.navOffCanvas = $rootScope.navOffCanvas ? false : true;
        console.log("navOffCanvas: " + $scope.navOffCanvas);

        $timeout(function() {
            $rootScope.$broadcast("c3.resize");
        }, 260);	// adjust this time according to nav transition
    };


    // ======= Site Settings
    $scope.toggleSettingsBox = function() {
        $scope.isSettingsOpen = $scope.isSettingsOpen ? false : true;
    };

    $scope.themeActive = "theme-zero";	// first theme

    $scope.fixedHeader = true;
    $scope.navHorizontal = false;	// this will access by other directive, so in rootScope.


    // === saving states
    var SETTINGS_STATES = "_setting-states";
    var statesQuery = {
        get : function() {
            return JSON.parse(localStorage.getItem(SETTINGS_STATES));
        },
        put : function(states) {
            localStorage.setItem(SETTINGS_STATES, JSON.stringify(states));
        }
    };

    // initialize the states
    var sQuery = statesQuery.get() || {
            navHorizontal: $scope.navHorizontal,
            fixedHeader: $scope.fixedHeader,
            navFull: $scope.navFull,
            themeActive: $scope.themeActive
        };
    // console.log(savedStates);
    if(sQuery) {
        $scope.navHorizontal = sQuery.navHorizontal;
        $scope.fixedHeader = sQuery.fixedHeader;
        $scope.navFull = sQuery.navFull;
        $scope.themeActive = sQuery.themeActive;
    }




    // putting the states
    $scope.onNavHorizontal = function() {
        sQuery.navHorizontal = $scope.navHorizontal;
        statesQuery.put(sQuery);
    };

    $scope.onNavFull = function() {
        sQuery.navFull = $scope.navFull;
        statesQuery.put(sQuery);

        $timeout(function() {
            $rootScope.$broadcast("c3.resize");
        }, 260);

    };

    $scope.onFixedHeader = function() {
        sQuery.fixedHeader = $scope.fixedHeader;
        statesQuery.put(sQuery);
    };

    $scope.onThemeActive = function() {
        sQuery.themeActive = $scope.themeActive;
        statesQuery.put(sQuery);
    };

    $scope.onThemeChange = function(theme) {
        $scope.themeActive = theme;
        $scope.onThemeActive();
    };



})
