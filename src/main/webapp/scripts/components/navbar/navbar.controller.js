'use strict';

angular.module('bombproofbillingApp')
    .controller('NavbarController', function ($scope, $state,Principal, Account) {
        $scope.isAuthenticated = Principal.isAuthenticated;
        $scope.$state = $state;
        $scope.account = Account.get(function() {
            $scope.highestRole();
        });
        $scope.role = null;

        $scope.highestRole = function() {
            for (var i = 0; i < $scope.account.roles.length; i++) {
                if ($scope.account.roles[i] === "ROLE_ADMIN") {
                    $scope.role = "Admin";
                    break;
                } else if ($scope.account.roles[i] === "ROLE_TRAINER") {
                    $scope.role = "Trainer";
                }

            }
        }

    });
