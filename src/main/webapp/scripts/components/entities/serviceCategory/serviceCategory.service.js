'use strict';

angular.module('bombproofbillingApp')
    .factory('ServiceCategory', function ($resource, DateUtils) {
        return $resource('api/serviceCategorys/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
