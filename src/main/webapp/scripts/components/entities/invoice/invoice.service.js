'use strict';

angular.module('bombproofbillingApp')
    .factory('Invoice', function ($resource, DateUtils) {
        return $resource('api/invoices/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.date = DateUtils.convertDateTimeFromServer(data.date);
                    return data;
                }
            },
            'update': { method:'PUT' },
            'findAllByTrainerId': {method: 'GET', isArray:true, params: {}}
        });
    });
