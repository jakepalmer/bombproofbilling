'use strict';

angular.module('bombproofbillingApp')
    .factory('Trainer', function ($resource, DateUtils) {
        return $resource('api/trainers/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'getByLogin': {method:'GET', url: 'api/trainerbylogin', params: {}},
            'updateClients': {method:'PUT', url: 'api/trainers/updateClients', params: {}},
            'updateServices': {method:'PUT', url: 'api/trainers/updateServices', params: {}},
            'update': { method:'PUT' }
        });
    });
