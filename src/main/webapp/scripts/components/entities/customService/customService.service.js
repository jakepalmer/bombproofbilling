'use strict';

angular.module('bombproofbillingApp')
    .factory('CustomService', function ($resource, DateUtils) {
        return $resource('api/customServices/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
