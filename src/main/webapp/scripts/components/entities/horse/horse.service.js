'use strict';

angular.module('bombproofbillingApp')
    .factory('Horse', function ($resource, DateUtils) {
        return $resource('api/horses/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
