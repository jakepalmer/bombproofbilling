'use strict';

angular.module('bombproofbillingApp')
    .factory('Admin', function ($resource) {
        return $resource('api/admins/:login', {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'getAdmins' : {url: "api/admins", method: 'GET', isArray: true},
            'deleteAdmin' : {url: "api/admins/:id", method: 'DELETE', isArray: false},
            'getById': {
                method: 'GET',
                url: "api/admins/details/:id",
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }

        });
    });
