/* globals $ */
'use strict';

angular.module('bombproofbillingApp')
    .directive('bombproofbillingAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
