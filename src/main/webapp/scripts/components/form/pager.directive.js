/* globals $ */
'use strict';

angular.module('bombproofbillingApp')
    .directive('bombproofbillingAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
