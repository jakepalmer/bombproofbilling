'use strict';

angular.module('bombproofbillingApp')
    .factory('Register', function ($resource) {
        return {
            registerClient: $resource('api/register/client', {}, {}),
            registerAdmin: $resource('api/register/admin', {}, {}),
            registerTrainer: $resource('api/register/trainer', {}, {})
        }
    });


