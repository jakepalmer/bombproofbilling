package com.bombproofbilling.repository;

import com.bombproofbilling.domain.CustomService;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the CustomService entity.
 */
public interface CustomServiceRepository extends JpaRepository<CustomService,Long> {

}
