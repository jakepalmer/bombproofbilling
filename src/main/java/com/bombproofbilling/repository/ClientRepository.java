package com.bombproofbilling.repository;

import com.bombproofbilling.domain.Client;
import com.bombproofbilling.domain.Horse;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Client entity.
 */
public interface ClientRepository extends JpaRepository<Client,Long> {

    @Query("select client from Client client where client.user.login = ?#{principal.username}")
    List<Client> findAllForCurrentUser();

    @Query("select client from Client client where client.user.login = ?1")
    Client findOneByUserLogin(String login);

}
