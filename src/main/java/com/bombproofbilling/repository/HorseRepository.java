package com.bombproofbilling.repository;

import com.bombproofbilling.domain.Horse;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Horse entity.
 */
public interface HorseRepository extends JpaRepository<Horse,Long> {

//    @Query("select horse from Horse horse left join fetch horse.client where horse.id =:id")
//    Horse findOneWithEagerRelationships(@Param("id") Long id);

}
