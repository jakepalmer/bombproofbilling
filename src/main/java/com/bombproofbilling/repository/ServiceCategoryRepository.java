package com.bombproofbilling.repository;

import com.bombproofbilling.domain.ServiceCategory;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ServiceCategory entity.
 */
public interface ServiceCategoryRepository extends JpaRepository<ServiceCategory,Long> {

}
