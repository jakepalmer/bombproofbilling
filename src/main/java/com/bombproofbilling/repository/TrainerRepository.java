package com.bombproofbilling.repository;

import com.bombproofbilling.domain.Trainer;
import com.bombproofbilling.domain.TrainerService;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

/**
 * Spring Data JPA repository for the Trainer entity.
 */
public interface TrainerRepository extends JpaRepository<Trainer,Long> {

    @Query("select trainer from Trainer trainer where trainer.user.login = ?#{principal.username}")
    List<Trainer> findAllForCurrentUser();

    @Query("select trainer from Trainer trainer where trainer.user.login = ?1")
    Trainer findOneByLogin(String login);

    @Query("select trainer from Trainer trainer left join FETCH trainer.trainerService where trainer.id= :id")
    Trainer findTrainerAndAllTrainerServiceById(@Param("id") Long id);

}
