package com.bombproofbilling.repository;

import com.bombproofbilling.domain.TrainerService;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Spring Data JPA repository for the TrainerService entity.
 */
public interface TrainerServiceRepository extends JpaRepository<TrainerService,Long> {

    Set<TrainerService> findAllByTrainerId(Long id);

    @Modifying
    @Transactional
    @Query(value="delete from TrainerService trainerservice where trainerservice.trainer.id = :id")
    void deleteAllByTrainerId(@Param("id") Long id);

    @Modifying
    @Transactional
    @Query(value="delete from TrainerTrainerService trainerservice where trainerservice.trainerId = :id")
    void deleteAllFromTrainerTrainerServiceByTrainerId(@Param("id") Long id);
}
