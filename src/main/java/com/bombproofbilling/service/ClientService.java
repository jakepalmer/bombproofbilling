package com.bombproofbilling.service;

import com.bombproofbilling.domain.Client;
import com.bombproofbilling.domain.Horse;
import com.bombproofbilling.domain.Trainer;
import com.bombproofbilling.domain.User;
import com.bombproofbilling.repository.ClientRepository;
import com.bombproofbilling.repository.TrainerRepository;
import com.bombproofbilling.web.rest.dto.ClientDTO;
import com.bombproofbilling.web.rest.dto.HorseDTO;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.Set;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class ClientService {

    private final Logger log = LoggerFactory.getLogger(ClientService.class);

    @Inject
    private ClientRepository clientRepository;

    @Inject
    private TrainerRepository trainerRepository;

    public Client createClient(ClientDTO clientDTO, User user){
        //Establish a new Client Entity
        Client client = new Client();
        client.setAddress(clientDTO.getAddress());
        client.setFirstName(clientDTO.getFirstName());
        client.setLastName(clientDTO.getLastName());
        client.setPhone(clientDTO.getPhone());
        client.setUser(user);

        //Add each horse for the user
        Set<Horse> horses = new HashSet<Horse>();
        //Add each horse for the user
        for (HorseDTO horse : clientDTO.getHorses()) {
            Horse newHorse = new Horse();
            newHorse.setName(horse.getName());
            newHorse.setBreed(horse.getBreed());
            if (StringUtils.isNotEmpty(horse.getUsef())){
                newHorse.setUsef(horse.getUsef());
            }
            horses.add(newHorse);

        }

        client.setHorses(horses);

        //Get the trainer that is associated to the client and save it to the Client
        Trainer clientTrainer = trainerRepository.findOne(clientDTO.getTrainer().getId());


        clientRepository.save(client);
        clientTrainer.getClient().add(client);
        trainerRepository.save(clientTrainer);

        log.debug("Created Information for Client: {}", client);
        return client;
    }
}
