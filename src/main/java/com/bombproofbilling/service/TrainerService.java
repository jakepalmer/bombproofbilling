package com.bombproofbilling.service;

import com.bombproofbilling.domain.Trainer;
import com.bombproofbilling.domain.User;
import com.bombproofbilling.repository.TrainerRepository;
import com.bombproofbilling.web.rest.dto.TrainerDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class TrainerService {

    private final Logger log = LoggerFactory.getLogger(TrainerService.class);

    @Inject
    private TrainerRepository trainerRepository;

    public Trainer createTrainer(User user, TrainerDTO trainerDTO){
        Trainer trainer = new Trainer();
        trainer.setAddress(trainerDTO.getAddress());
        trainer.setPhone(trainerDTO.getPhone());
        trainer.setFirstName(user.getFirstName());
        trainer.setLastName(user.getLastName());
        trainer.setUser(user);

        trainerRepository.save(trainer);
        log.debug("Created Information for Trainer: {}", trainer);
        return trainer;
    }
}
