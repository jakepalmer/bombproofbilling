/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.bombproofbilling.web.rest.dto;
