package com.bombproofbilling.web.rest.dto;

import com.bombproofbilling.domain.TrainerService;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

public class TrainerDTO {

    @Pattern(regexp = "^[2-9]{1}[0-9]{9}$")
    @NotNull
    @Size(min = 10, max = 10)
    private String phone;

    @NotNull
    private String address;

    @NotNull
    @Size(max = 50)
    private String firstName;

    @NotNull
    @Size(max = 50)
    private String lastName;

    private UserDTO user;

    private List<ServiceDTO> trainerService;

    private Set<ClientDTO> client;

    private Long id;

    public TrainerDTO() {
    }

    public Long getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public UserDTO getUser() {
        return user;
    }

    public Set<ClientDTO> getClient() {
        return client;
    }

    public List<ServiceDTO> getTrainerService(){return trainerService;}

    @Override
    public String toString() {
        return "TrainerDTO{" +
        "phone='" + phone + '\'' +
        ", id='" + id + '\'' +
        ", address='" + address + '\'' +
        ", firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", client='" + client + '\'' +
        ", user=" + user +
        ", services=" + trainerService +
        '}';
    }
}
