package com.bombproofbilling.web.rest.dto;

import javax.validation.constraints.NotNull;

public class HorseDTO {


    @NotNull
    private String name;

    @NotNull
    private String breed;

    private String usef;

    public HorseDTO() {
    }

    public String getName() {
        return name;
    }

    public String getBreed() {
        return breed;
    }

    public String getUsef() {
        return usef;
    }

    @Override
    public String toString() {
        return "HorseDTO{" +
            "name='" + name + '\'' +
            ", breed='" + breed + '\'' +
            ", usef='" + usef + '\'' +
            '}';
    }
}
