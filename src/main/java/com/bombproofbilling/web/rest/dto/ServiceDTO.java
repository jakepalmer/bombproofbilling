package com.bombproofbilling.web.rest.dto;

import java.math.BigDecimal;

public class ServiceDTO {

        private Long id;
        private String name;
        private BigDecimal cost;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    @Override
        public String toString() {
            return "ServiceDTO{" +
                "id=" + id +
                ", name='" + name + "'" +
                ", cost='" + cost + "'" +
                '}';
        }
}
