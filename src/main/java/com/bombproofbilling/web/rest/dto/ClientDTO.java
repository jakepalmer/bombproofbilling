package com.bombproofbilling.web.rest.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;

public class ClientDTO {

    @Pattern(regexp = "^[2-9]{1}[0-9]{9}$")
    @NotNull
    @Size(min = 10, max = 10)
    private String phone;

    @NotNull
    private String address;

    @NotNull
    @Size(max = 50)
    private String firstName;

    @NotNull
    @Size(max = 50)
    private String lastName;

    @NotNull
    private UserDTO user;

    private Set<HorseDTO> horses;

    private TrainerDTO trainer;

    public ClientDTO() {
    }


    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }


    public Set<HorseDTO> getHorses() {
        return horses;
    }

    public UserDTO getUser() {
        return user;
    }

    public TrainerDTO getTrainer() {
        return trainer;
    }

    @Override
    public String toString() {
        return "ClientDTO{" +
            "phone='" + phone + '\'' +
            ", address='" + address + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", user=" + user +
            ", horses=" + horses +
            ", trainer=" + trainer +
            '}';
    }
}
