package com.bombproofbilling.web.rest;

import com.bombproofbilling.domain.Client;
import com.bombproofbilling.domain.Trainer;
import com.bombproofbilling.domain.User;
import com.bombproofbilling.repository.ClientRepository;
import com.bombproofbilling.repository.TrainerRepository;
import com.bombproofbilling.repository.UserRepository;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * REST controller for managing Client.
 */
@RestController
@RequestMapping("/api")
public class ClientResource {

    private final Logger log = LoggerFactory.getLogger(ClientResource.class);

    @Inject
    private ClientRepository clientRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private TrainerRepository trainerRepository;

    /**
     * POST  /clients -> Create a new client.
     */
    @RequestMapping(value = "/clients",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Client> create(@Valid @RequestBody Client client) throws URISyntaxException {
        log.debug("REST request to save Client : {}", client);
        if (client.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new client cannot already have an ID").body(null);
        }
        Client result = clientRepository.save(client);
        return ResponseEntity.created(new URI("/api/clients/" + client.getId())).body(result);
    }

    /**
     * PUT  /clients -> Updates an existing client.
     */
    @RequestMapping(value = "/clients",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Client> update(@Valid @RequestBody Client client) throws URISyntaxException {
        log.debug("REST request to update Client : {}", client);
        if (client.getId() == null) {
            return create(client);
        }
        Client result = clientRepository.save(client);
        return ResponseEntity.ok().body(result);
    }

    /**
     * GET  /clients -> get all the clients.
     */
    @RequestMapping(value = "/clients",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Client> getAll() {
        log.debug("REST request to get all Clients");
        List<Client> clients = clientRepository.findAll(sortByFirstNameAsc());
        return clients;
    }

    /**
     * GET  /clients/:id -> get the "id" client.
     */
    @RequestMapping(value = "/clients/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Client> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Client : {}", id);
        Client client = clientRepository.findOne(id);
        if (client == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    /**
     * DELETE  /clients/:id -> delete the "id" client.
     */
    @RequestMapping(value = "/clients/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Client : {}", id);
        Client client = clientRepository.findOne(id);
        List<Trainer> trainers = trainerRepository.findAll();
        List<Trainer> trainersToSaveAfterRemovingClient = new ArrayList<>();
        for (Trainer trainer: trainers) {
            if (trainer.getClient().contains(client)) {
                trainer.getClient().remove(client);
                trainersToSaveAfterRemovingClient.add(trainer);
                break;
            }
        }
        trainerRepository.save(trainersToSaveAfterRemovingClient);
        User user = client.getUser();
        clientRepository.delete(id);
        userRepository.delete(user.getId());
    }

    private Sort sortByFirstNameAsc() {
        return new Sort(Sort.Direction.ASC, "firstName");
    }
}
