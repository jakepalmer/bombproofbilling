package com.bombproofbilling.web.rest;

import com.bombproofbilling.domain.Service;
import com.bombproofbilling.domain.Trainer;
import com.bombproofbilling.domain.TrainerService;
import com.bombproofbilling.domain.User;
import com.bombproofbilling.repository.*;
import com.bombproofbilling.web.rest.dto.ClientDTO;
import com.bombproofbilling.web.rest.dto.ServiceDTO;
import com.bombproofbilling.web.rest.dto.TrainerDTO;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * REST controller for managing Trainer.
 */
@RestController
@RequestMapping("/api")
public class TrainerResource {

    private final Logger log = LoggerFactory.getLogger(TrainerResource.class);

    @Inject
    private TrainerRepository trainerRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private ClientRepository clientRepository;

    @Inject
    private ServiceRepository serviceRepository;

    @Inject
    private TrainerServiceRepository trainerServiceRepository;

    /**
     * POST  /trainers -> Create a new trainer.
     */
    @RequestMapping(value = "/trainers",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Trainer> create(@Valid @RequestBody Trainer trainer) throws URISyntaxException {
        log.debug("REST request to save Trainer : {}", trainer);
        if (trainer.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new trainer cannot already have an ID").body(null);
        }
        Trainer result = trainerRepository.save(trainer);
        return ResponseEntity.created(new URI("/api/trainers/" + trainer.getId())).body(result);
    }

    /**
     * PUT  /trainers -> Updates an existing trainer.
     */
    @RequestMapping(value = "/trainers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Trainer> update(@Valid @RequestBody Trainer trainer) throws URISyntaxException {
        log.debug("REST request to update Trainer : {}", trainer);
        if (trainer.getId() == null) {
            return create(trainer);
        }
        Trainer result = trainerRepository.save(trainer);
        return ResponseEntity.ok().body(result);
    }

    /**
     * PUT  /trainers/updateClients -> Updates an existing trainer.
     */
    @RequestMapping(value = "/trainers/updateClients",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Trainer> updateClients(@RequestBody TrainerDTO trainer) throws URISyntaxException {
        Trainer trainerToUpdate = trainerRepository.findOne(trainer.getId());
        log.debug("REST request to update Trainer Clients: {}", trainer);

        if (trainerToUpdate != null) {
            trainerToUpdate.getClient().clear();
            for (ClientDTO client: trainer.getClient()) {
                trainerToUpdate.getClient().add(clientRepository.findOneByUserLogin(client.getUser().getLogin()));
            }
            Trainer result = trainerRepository.save(trainerToUpdate);
            return ResponseEntity.ok().body(result);
        } else {
            return ResponseEntity.unprocessableEntity().body(null);
        }

    }

    /**
     * PUT  /trainers/updateClients -> Updates an existing trainer.
     */
    @RequestMapping(value = "/trainers/updateServices",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Trainer> updateServices(@RequestBody TrainerDTO trainer) throws URISyntaxException {
        Trainer trainerToUpdate = trainerRepository.findOne(trainer.getId());
        log.debug("REST request to update Trainer Services: {}", trainer);

        List<TrainerService> trainerServicesToSave = new ArrayList<>();
        if (trainerToUpdate != null) {
            trainerToUpdate.setTrainerService(new HashSet<TrainerService>());
            for (ServiceDTO service : trainer.getTrainerService()) {
                Service retrievedService = serviceRepository.findOne(service.getId());
                if (retrievedService == null) {
                    TrainerService retrievedTrainerService = trainerServiceRepository.findOne(service.getId());
                    retrievedTrainerService.setCost(service.getCost());
                    retrievedTrainerService.setTrainer(trainerToUpdate);
                    trainerServicesToSave.add(retrievedTrainerService);
                } else {
                    TrainerService trainerService = new TrainerService();
                    trainerService.setCost(service.getCost());
                    trainerService.setName(retrievedService.getName());
                    trainerService.setService(retrievedService);
                    trainerService.setTrainer(trainerToUpdate);
                    trainerToUpdate.getTrainerService().add(trainerService);
                    trainerServicesToSave.add(trainerService);
                }
            }
            trainerServiceRepository.deleteAllFromTrainerTrainerServiceByTrainerId(trainer.getId());
            trainerServiceRepository.deleteAllByTrainerId(trainer.getId());
            trainerServiceRepository.save(trainerServicesToSave);
            Trainer result = trainerRepository.save(trainerToUpdate);
            result.setTrainerService(trainerServiceRepository.findAllByTrainerId(result.getId()));
            return ResponseEntity.ok().body(result);
        } else {
            return ResponseEntity.unprocessableEntity().body(null);
        }

    }

    /**
     * GET  /trainers -> get all the trainers.
     */
    @RequestMapping(value = "/trainers",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Trainer> getAll() {
        log.debug("REST request to get all Trainers");
        return trainerRepository.findAll();
    }

    /**
     * GET /trainers -> get trainer by login
     */
    @RequestMapping(value = "/trainerbylogin",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Trainer getTrainerByLogin(@RequestParam String login) {
        log.debug("REST request to get Trainer by login {}", login);
        Trainer trainerByLogin = trainerRepository.findOneByLogin(login);
        trainerByLogin.setTrainerService(trainerServiceRepository.findAllByTrainerId(trainerByLogin.getId()));
        return trainerByLogin;
    }

    /**
     * GET  /trainers/:id -> get the "id" trainer.
     */
    @RequestMapping(value = "/trainers/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Trainer> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Trainer : {}", id);
        Trainer trainer = trainerRepository.findTrainerAndAllTrainerServiceById(id);
        if (trainer == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(trainer, HttpStatus.OK);
    }

    /**
     * DELETE  /trainers/:id -> delete the "id" trainer.
     */
    @RequestMapping(value = "/trainers/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Trainer : {}", id);

        Trainer trainer = trainerRepository.findOne(id);
        User user = trainer.getUser();
        trainerRepository.delete(id);
        userRepository.delete(user.getId());
    }
}
