package com.bombproofbilling.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.bombproofbilling.domain.CustomService;
import com.bombproofbilling.repository.CustomServiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing CustomService.
 */
@RestController
@RequestMapping("/api")
public class CustomServiceResource {

    private final Logger log = LoggerFactory.getLogger(CustomServiceResource.class);

    @Inject
    private CustomServiceRepository customServiceRepository;

    /**
     * POST  /customServices -> Create a new customService.
     */
    @RequestMapping(value = "/customServices",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CustomService> create(@Valid @RequestBody CustomService customService) throws URISyntaxException {
        log.debug("REST request to save CustomService : {}", customService);
        if (customService.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new customService cannot already have an ID").body(null);
        }
        CustomService result = customServiceRepository.save(customService);
        return ResponseEntity.created(new URI("/api/customServices/" + customService.getId())).body(result);
    }

    /**
     * PUT  /customServices -> Updates an existing customService.
     */
    @RequestMapping(value = "/customServices",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CustomService> update(@Valid @RequestBody CustomService customService) throws URISyntaxException {
        log.debug("REST request to update CustomService : {}", customService);
        if (customService.getId() == null) {
            return create(customService);
        }
        CustomService result = customServiceRepository.save(customService);
        return ResponseEntity.ok().body(result);
    }

    /**
     * GET  /customServices -> get all the customServices.
     */
    @RequestMapping(value = "/customServices",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<CustomService> getAll() {
        log.debug("REST request to get all CustomServices");
        return customServiceRepository.findAll();
    }

    /**
     * GET  /customServices/:id -> get the "id" customService.
     */
    @RequestMapping(value = "/customServices/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CustomService> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get CustomService : {}", id);
        CustomService customService = customServiceRepository.findOne(id);
        if (customService == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(customService, HttpStatus.OK);
    }

    /**
     * DELETE  /customServices/:id -> delete the "id" customService.
     */
    @RequestMapping(value = "/customServices/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete CustomService : {}", id);
        customServiceRepository.delete(id);
    }
}
