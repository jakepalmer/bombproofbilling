package com.bombproofbilling.web.rest;

import com.bombproofbilling.domain.Admin;
import com.bombproofbilling.domain.Authority;
import com.bombproofbilling.domain.User;
import com.bombproofbilling.repository.UserRepository;
import com.bombproofbilling.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * REST controller for managing users.
 */
@RestController
@RequestMapping("/api")
public class AdminResource {

    private final Logger log = LoggerFactory.getLogger(AdminResource.class);

    @Inject
    private UserRepository userRepository;

    /**
     * GET  /admin -> get all admins.
     */
    @RequestMapping(value = "/admins",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<User> getAll() {
        log.debug("REST request to get all Admins");
        List<User> users = userRepository.findAll();
        List<User> adminUsers = new ArrayList<>();
        for (User user: users) {
            for (Authority authority: user.getAuthorities()) {
                if (authority.getName().equals(AuthoritiesConstants.ADMIN)){
                    adminUsers.add(user);
                    break;
                }
            }
        }
        return adminUsers;
    }

    /**
     * GET  /admins/:login -> get the "login" user.
     */
    @RequestMapping(value = "/admins/{login}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public User getAdmin(@PathVariable String login, HttpServletResponse response) {
        log.debug("REST request to get Admin : {}", login);
        User user = userRepository.findOneByLogin(login);
        if (user == null) {
            user = userRepository.findOneByEmail(login);
        }
        if (user == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return user;
    }

    /**
     * GET  /admins/:login -> get the "login" user.
     */
    @RequestMapping(value = "/admins/details/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public User getAdmin(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Admin by id : {}", id);
        User user = userRepository.findOne(id);
        if (user == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return user;
    }

    /**
     * PUT  /clients -> Updates an existing client.
     */
    @RequestMapping(value = "/admins",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<User> update(@Valid @RequestBody Admin admin, HttpServletResponse response) throws URISyntaxException {
        log.debug("REST request to update Admin : {}", admin);
        User existingUser = userRepository.findOneByLogin(admin.getLogin());
        if (existingUser == null) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } else {
            if (StringUtils.isNotEmpty(admin.getFirstName())) {
                existingUser.setFirstName(admin.getFirstName());
            }
            if (StringUtils.isNotEmpty(admin.getLastName())) {
                existingUser.setLastName(admin.getLastName());
            }
            if (StringUtils.isNotEmpty(admin.getEmail())) {
                existingUser.setEmail(admin.getEmail());
            }
            existingUser.setLastModifiedDate(new DateTime());
        }
        User result = userRepository.save(existingUser);
        return ResponseEntity.ok().body(result);
    }

    /**
     * DELETE  /admins/:login -> delete the "id" client.
     */
    @RequestMapping(value = "/admins/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Admin : {}", id);
        userRepository.delete(id);
    }

}
