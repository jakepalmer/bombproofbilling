package com.bombproofbilling.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;


/**
 * A TrainerTrainerService.
 */
@Entity
@Table(name = "TRAINER_TRAINERSERVICE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TrainerTrainerService implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "trainer_id")
    private Long trainerId;

    @Column(name = "trainerservice_id")
    private Long trainerServiceId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(Long trainerId) {
        this.trainerId = trainerId;
    }

    public Long getTrainerServiceId() {
        return trainerServiceId;
    }

    public void setTrainerServiceId(Long trainerServiceId) {
        this.trainerServiceId = trainerServiceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TrainerTrainerService trainerService = (TrainerTrainerService) o;

        if ( ! Objects.equals(id, trainerService.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TrainerService{" +
                "id=" + id +
                ", trainerId='" + trainerId + "'" +
                ", trainerServiceId='" + trainerServiceId + "'" +
                '}';
    }
}
