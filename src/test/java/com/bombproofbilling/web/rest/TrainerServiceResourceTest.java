package com.bombproofbilling.web.rest;

import com.bombproofbilling.Application;
import com.bombproofbilling.domain.TrainerService;
import com.bombproofbilling.repository.TrainerServiceRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TrainerServiceResource REST controller.
 *
 * @see TrainerServiceResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class TrainerServiceResourceTest {

    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";

    private static final BigDecimal DEFAULT_COST = new BigDecimal(0);
    private static final BigDecimal UPDATED_COST = new BigDecimal(1);

    @Inject
    private TrainerServiceRepository trainerServiceRepository;

    private MockMvc restTrainerServiceMockMvc;

    private TrainerService trainerService;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TrainerServiceResource trainerServiceResource = new TrainerServiceResource();
        ReflectionTestUtils.setField(trainerServiceResource, "trainerServiceRepository", trainerServiceRepository);
        this.restTrainerServiceMockMvc = MockMvcBuilders.standaloneSetup(trainerServiceResource).build();
    }

    @Before
    public void initTest() {
        trainerService = new TrainerService();
        trainerService.setName(DEFAULT_NAME);
        trainerService.setCost(DEFAULT_COST);
    }

    @Test
    @Transactional
    public void createTrainerService() throws Exception {
        int databaseSizeBeforeCreate = trainerServiceRepository.findAll().size();

        // Create the TrainerService
        restTrainerServiceMockMvc.perform(post("/api/trainerServices")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(trainerService)))
                .andExpect(status().isCreated());

        // Validate the TrainerService in the database
        List<TrainerService> trainerServices = trainerServiceRepository.findAll();
        assertThat(trainerServices).hasSize(databaseSizeBeforeCreate + 1);
        TrainerService testTrainerService = trainerServices.get(trainerServices.size() - 1);
        assertThat(testTrainerService.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTrainerService.getCost()).isEqualTo(DEFAULT_COST);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = trainerServiceRepository.findAll().size();
        // set the field null
        trainerService.setName(null);

        // Create the TrainerService, which fails.
        restTrainerServiceMockMvc.perform(post("/api/trainerServices")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(trainerService)))
                .andExpect(status().isBadRequest());

        List<TrainerService> trainerServices = trainerServiceRepository.findAll();
        assertThat(trainerServices).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCostIsRequired() throws Exception {
        int databaseSizeBeforeTest = trainerServiceRepository.findAll().size();
        // set the field null
        trainerService.setCost(null);

        // Create the TrainerService, which fails.
        restTrainerServiceMockMvc.perform(post("/api/trainerServices")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(trainerService)))
                .andExpect(status().isBadRequest());

        List<TrainerService> trainerServices = trainerServiceRepository.findAll();
        assertThat(trainerServices).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTrainerServices() throws Exception {
        // Initialize the database
        trainerServiceRepository.saveAndFlush(trainerService);

        // Get all the trainerServices
        restTrainerServiceMockMvc.perform(get("/api/trainerServices"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(trainerService.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].cost").value(hasItem(DEFAULT_COST.intValue())));
    }

    @Test
    @Transactional
    public void getTrainerService() throws Exception {
        // Initialize the database
        trainerServiceRepository.saveAndFlush(trainerService);

        // Get the trainerService
        restTrainerServiceMockMvc.perform(get("/api/trainerServices/{id}", trainerService.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(trainerService.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.cost").value(DEFAULT_COST.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTrainerService() throws Exception {
        // Get the trainerService
        restTrainerServiceMockMvc.perform(get("/api/trainerServices/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTrainerService() throws Exception {
        // Initialize the database
        trainerServiceRepository.saveAndFlush(trainerService);

		int databaseSizeBeforeUpdate = trainerServiceRepository.findAll().size();

        // Update the trainerService
        trainerService.setName(UPDATED_NAME);
        trainerService.setCost(UPDATED_COST);
        restTrainerServiceMockMvc.perform(put("/api/trainerServices")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(trainerService)))
                .andExpect(status().isOk());

        // Validate the TrainerService in the database
        List<TrainerService> trainerServices = trainerServiceRepository.findAll();
        assertThat(trainerServices).hasSize(databaseSizeBeforeUpdate);
        TrainerService testTrainerService = trainerServices.get(trainerServices.size() - 1);
        assertThat(testTrainerService.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTrainerService.getCost()).isEqualTo(UPDATED_COST);
    }

    @Test
    @Transactional
    public void deleteTrainerService() throws Exception {
        // Initialize the database
        trainerServiceRepository.saveAndFlush(trainerService);

		int databaseSizeBeforeDelete = trainerServiceRepository.findAll().size();

        // Get the trainerService
        restTrainerServiceMockMvc.perform(delete("/api/trainerServices/{id}", trainerService.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TrainerService> trainerServices = trainerServiceRepository.findAll();
        assertThat(trainerServices).hasSize(databaseSizeBeforeDelete - 1);
    }
}
